FROM continuumio/miniconda3

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN mkdir -p /backend
RUN mkdir -p /scripts
RUN mkdir -p /frontend

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash - && apt-get install -y nodejs


COPY ./backend/requirements.yml /backend/requirements.yml

COPY ./scripts /scripts
RUN chmod +x /scripts

RUN /opt/conda/bin/conda env create -f /backend/requirements.yml

ENV PYTHONDONTWRITEBYTECODE=1
ENV PATH /opt/conda/envs/backend_env/bin:$PATH
RUN echo "source activate backend_env">~/.bashrc

WORKDIR /frontend
COPY ./frontend/frontend-assignment-main/package.json /frontend/
COPY ./frontend/frontend-assignment-main/package-lock.json /frontend/
RUN npm install
COPY ./frontend/frontend-assignment-main/ /frontend
RUN npm run build

COPY ./backend /backend
WORKDIR /backend
