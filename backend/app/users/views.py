from app.social.permissions import IsOwnerOrReadOnly
from app.users.serializers import UserSerializer
from django.contrib.auth import get_user_model

from app.users.permissions import IsAdmin, ReadOnly
from django.db.models import Q
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView

User = get_user_model()


class ListUsers(ListAPIView):
    """
    List all Users.
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        if 'search' in self.request.query_params.keys():
            queryset = User.objects.all()
            search_param = self.request.query_params['search']
            queryset = queryset.filter(Q(first_name__icontains=search_param) | Q(last_name__icontains=search_param))
            return queryset
        else:
            return User.objects.all()


class GetUpdateDeleteUserAPIView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'id'
    permission_classes = [IsAdmin | ReadOnly]


class UserMe(RetrieveUpdateDestroyAPIView):
    """
    get:
    Get User's Profile

    # Get User's Profile
    Get logged-in user’s profile (as well as private information like email, etc.)

    patch:
    Update User's Profile

    # Update User's Profile
     Update the logged-in user’s profile public info

    Delete:
    Delete User's Profile

    # Delete User's Profile
     Delete the logged-in user’s profile and all related data (posts, comments, likes, etc.)
    """
    serializer_class = UserSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def get_object(self):
        return self.request.user
