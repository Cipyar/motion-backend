from app.users.views import ListUsers, UserMe, GetUpdateDeleteUserAPIView
from django.urls import path

urlpatterns = [
    path('', ListUsers.as_view(), name='list-users'),
    path('<int:id>/', GetUpdateDeleteUserAPIView.as_view()),
    path('me/', UserMe.as_view(), name='me-user'),
]
