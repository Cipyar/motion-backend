from rest_framework import serializers

from app.friend_request.models import FriendRequests


class FriendRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = FriendRequests
        fields = "__all__"
        read_only_fields = ["requester", "receiver"]
