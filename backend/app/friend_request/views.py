from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from app.users.models import User
from .models import FriendRequests
from .permissions import SenderNotReceiver, IsInvolvedInFriendRequest
from .serializers import FriendRequestSerializer
from ..users.permissions import IsAdmin
from ..users.serializers import UserSerializer


class CreateFriendRequest(CreateAPIView):
    """
    post:
    Create a new friend request.
    """
    permission_classes = [SenderNotReceiver]
    serializer_class = FriendRequestSerializer
    lookup_url_kwarg = 'user_id'

    def perform_create(self, serializer):
        requester = self.request.user
        user_id = self.kwargs.get(self.lookup_url_kwarg)
        receiver = get_object_or_404(User, id=user_id)

        friend_requests = FriendRequests.objects.filter(
            Q(requester=requester, receiver=receiver)
            | Q(requester=receiver, receiver=requester)
        )

        if friend_requests.exists():
            raise ValidationError("This friend request already exists")

        serializer.save(requester=requester, receiver=receiver)


class RetrievePatchDeleteFriendRequest(RetrieveUpdateDestroyAPIView):
    """
    get:
    Retrieve a specific friend request.

    patch:
    Update the status of a friend request.

    delete:
    Delete a friend request.
    """
    permission_classes = [IsInvolvedInFriendRequest | IsAdmin]
    lookup_url_kwarg = 'friend_request_id'
    queryset = FriendRequests.objects.all()
    serializer_class = FriendRequestSerializer


class ListAcceptedFriends(ListAPIView):
    """
    get:
    List accepted friend requests for the logged-in user.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self):
        user = self.request.user
        accepted_requests = FriendRequests.objects.filter(Q(requester=user) | Q(receiver=user), status="A")
        return User.objects.filter(
            (Q(sent_requests__in=accepted_requests) | Q(received_requests__in=accepted_requests)) & ~Q(
                id=user.id)).distinct()
