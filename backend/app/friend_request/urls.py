from django.urls import path

from .views import (
    CreateFriendRequest,
    ListAcceptedFriends, RetrievePatchDeleteFriendRequest,
)

urlpatterns = [
    # POST: Send friend request to another user
    path('request/<int:user_id>/', CreateFriendRequest.as_view(), name='friend-request-create'),

    # GET, PATCH, DELETE: Manage details, acceptance/rejection, and deletion of a friend request
    path('requests/<int:friend_request_id>/', RetrievePatchDeleteFriendRequest.as_view(), name='friend-request-detail'),

    # GET: List all accepted friends
    path('', ListAcceptedFriends.as_view(), name='list-accepted-friends'),
]
