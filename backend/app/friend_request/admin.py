from django.contrib import admin

from .models import FriendRequests


@admin.register(FriendRequests)
class FriendRequestAdmin(admin.ModelAdmin):
    list_display = ('requester', 'receiver', 'status')
