from rest_framework.permissions import BasePermission


class SenderNotReceiver(BasePermission):
    message = "You cannot send a friend request to yourself."

    def has_permission(self, request, view):
        if 'requester' in request.data and 'receiver' in request.data:
            return request.data.get('requester') != request.data.get('receiver')
        return True


class IsInvolvedInFriendRequest(BasePermission):
    message = "You are not involved in this friend request."

    def has_object_permission(self, request, view, obj):
        return request.user == obj.requester or request.user == obj.receiver
