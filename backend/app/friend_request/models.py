from django.db import models

from app.users.models import User


class FriendRequests(models.Model):
    requester = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='sent_requests')
    receiver = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='received_requests')
    STATUS_CHOICES = [
        ('P´', 'Pending'),
        ('A', 'Accepted'),
        ('D', 'Declined'),
    ]
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='P')
