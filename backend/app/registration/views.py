from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from app.registration.models import RegistrationProfile, code_generator
from app.registration.permissions import IsNewUser, IsRegisteringUser
from app.users.serializers import RegisterUserSerializer, ValidationUserSerializer

User = get_user_model()


class CreateUser(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterUserSerializer
    permission_classes = [IsNewUser]

    def perform_create(self, serializer):
        email = self.request.data.get("email")
        serializer.save(email=email)
        ValiCode = RegistrationProfile.objects.get(user__email=email).code

        send_mail(
            'Validation for your registration',
            f'This is your validation code: {ValiCode} ',
            'locomotion.backend@gmail.com',
            [email],
            fail_silently=False,
        )


class CompleteRegistration(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = ValidationUserSerializer
    permission_classes = [IsRegisteringUser]

    def post(self, request, *args, **kwargs):
        # Check for Passwords
        if self.request.data.get('password') != self.request.data.get('password_repeat'):
            raise ValidationError({'password': 'Your passwords are not matching!'})
        # Check if Code is valid
        email = self.request.data.get("email")
        RegProfile = RegistrationProfile.objects.get(user__email=email)
        validationCode = RegProfile.code
        if validationCode != self.request.data.get('code'):
            raise ValidationError({'Validationcode': 'The validation code is incorrect'})

        user = User.objects.get(email=email)
        user.username = self.request.data.get("username")

        password = self.request.data.get("password")
        user.set_password(password)

        user.first_name = self.request.data.get("first_name")
        user.last_name = self.request.data.get("last_name")
        user.save()

        RegProfile.used = True
        RegProfile.save()

        return Response({'success': 'Account created successfully.'})


code_generator(length=5)


class ResetPassword(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterUserSerializer
    permission_classes = [IsNewUser]

    def perform_create(self, serializer):
        email = self.request.data.get("email")
        RegProfile = RegistrationProfile.objects.get(user__email=email)
        RegProfile.code = code_generator(length=5)
        RegProfile.used = False
        RegProfile.save()
        ValiCode = RegistrationProfile.objects.get(user__email=email).code

        send_mail(
            'Validation for your password reset',
            f'This is your reset code: {ValiCode} ',
            'locomotion.backend@gmail.com',
            [email],
            fail_silently=False,
        )


class ChangePassword(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = ValidationUserSerializer
    permission_classes = [IsRegisteringUser]

    def post(self, request, *args, **kwargs):
        if self.request.data.get('password') != self.request.data.get('password_repeat'):
            raise ValidationError({'password': 'Your passwords are not matching!'})
        # Check if Code is valid
        email = self.request.data.get("email")
        RegProfile = RegistrationProfile.objects.get(user__email=email)
        validationCode = RegProfile.code
        if validationCode != self.request.data.get('code'):
            raise ValidationError({'Validationcode': 'The validation code is incorrect'})

        user = User.objects.get(email=email)
        password = self.request.data.get("password")
        user.set_password(password)

        RegProfile.used = True
        RegProfile.save()

        user.save()

        return Response({'success': 'Password has been changed.'})
