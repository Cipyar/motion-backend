from django.contrib import admin

from app.registration.models import RegistrationProfile

# Register your models here.
admin.site.register(RegistrationProfile)
