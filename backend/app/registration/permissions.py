from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission

User = get_user_model()


class IsNewUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user not in User.objects.all()


class IsRegisteringUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return request.user in User.objects.all()
