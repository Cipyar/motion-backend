from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView, TokenObtainPairView

from app.registration.views import CreateUser, CompleteRegistration, ResetPassword, ChangePassword

app_name = 'registration'

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='retrieve-refreshed-token'),
    path('token/verify/', TokenVerifyView.as_view(), name='verify-token'),

    path('registration/', CreateUser.as_view(), name='create-user'),
    path('registration/validation/', CompleteRegistration.as_view(), name='complete-registration'),

    path('password-reset/', ResetPassword.as_view(), name='reset-password'),
    path('password-reset/validation/', ChangePassword.as_view(), name='change-password'),
]
