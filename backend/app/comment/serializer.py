from rest_framework import serializers

from app.comment.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id', 'text_content', "commented_post", "author", 'created', 'updated']
        read_only_fields = ['created']
