from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


# Create your models here.
class Comment(models.Model):
    text_content = models.TextField(max_length=140)
    commented_post = models.ForeignKey(to="social.Post", on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='authored_comments')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.text_content
