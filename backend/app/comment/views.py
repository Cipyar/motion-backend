from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated

from app.comment.models import Comment
from app.comment.serializer import CommentSerializer
from app.social.models import Post


# Create your views here.
class ListCreateComments(ListCreateAPIView):
    """
    get:
    List Comments of a Post

    # List Comments of a Post
    Provide Post ID to get all comments related to that post

    put:
    Write Comment

    # Write Comment
     Write a new comment for a post

    """
    serializer_class = CommentSerializer
    lookup_field = 'post_id'
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        post_id = self.kwargs["post_id"]
        queryset = Comment.objects.filter(commented_post=post_id)
        return queryset

    def perform_create(self, serializer):
        post_id = self.kwargs["post_id"]
        post_instance = Post.objects.get(id=post_id)
        serializer.save(author=self.request.user, commented_post=post_instance)
