# Generated by Django 4.2.5 on 2023-10-17 08:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("social", "0002_post_user"),
    ]

    operations = [
        migrations.AddField(
            model_name="post",
            name="shared_content",
            field=models.TextField(
                blank=True, null=True, verbose_name="Shared Content"
            ),
        ),
        migrations.AddField(
            model_name="post",
            name="shared_post",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="shared_posts",
                to="social.post",
                verbose_name="shared post",
            ),
        ),
    ]
