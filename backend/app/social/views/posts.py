from django.core.mail import send_mail
from rest_framework.generics import ListAPIView, GenericAPIView, ListCreateAPIView, \
    RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from app.social.models.posts import Post
from app.social.permissions import IsOwnerOrReadOnly, IsNotOwner
from app.social.serializers.posts import PostSerializer


class ListCreatePosts(ListCreateAPIView):
    """
    get:
    List all Posts.  dfbfdbdfbdf
    post:
    Create a new Post.
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        if 'search' in self.request.query_params.keys():
            queryset = Post.objects.all()
            search_content = self.request.query_params['search']
            queryset = queryset.filter(content__icontains=search_content)
            return queryset
        else:
            return Post.objects.all()

    def perform_create(self, serializer):
        serializer.save()
        user = self.request.user
        followers = self.request.user.followers.all()
        for follower in followers:
            send_mail(
                f'{user} just posted something',
                f' You are following {user} and they just posted something!',
                'locomotion.backend@gmail.com',
                [follower.email],
                fail_silently=False,
            )


class RetrieveUpdateDestroyPost(RetrieveUpdateDestroyAPIView):
    """
    get:
    Retrieve Post.

    patch:
    Update Post.

    delete:
    Delete Post.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    lookup_url_kwarg = 'post_id'
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]


class ListPostsUser(ListAPIView):
    """
    get:
    List al posts of a specific User.
    """
    serializer_class = PostSerializer
    lookup_url_kwarg = 'user_id'

    def get_queryset(self):
        user_id = self.kwargs.get("user_id")
        return Post.objects.filter(user__id=user_id).order_by("-created")


class ListPostsFollowees(ListAPIView):
    """
    get:
    List all Posts of Users the logged-in User follows.
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        followed_user_ids = self.request.user.followees.all().values_list("id", flat=True)
        posts = Post.objects.filter(user__in=followed_user_ids)
        return posts


class ListLikes(ListAPIView):
    """
    get:
    List all Posts bookmarked by logged-in User.
    """
    serializer_class = PostSerializer

    def get_queryset(self):
        return self.request.user.posts


class CreateLike(GenericAPIView):
    """
    post:
    Like Post for logged-in User.
    """
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_url_kwarg = 'post_id'
    permission_classes = [IsAuthenticated, IsNotOwner]

    def post(self, request, post_id):
        # get_object will return the object from the provided queryset that matches the post_id from the url
        post_to_save = self.get_object()
        user = request.user
        if post_to_save in user.liked_posts.all():
            user.liked_posts.remove(post_to_save)
            return Response(self.get_serializer(instance=post_to_save).data)
        user.liked_posts.add(post_to_save)
        return Response(self.get_serializer(instance=post_to_save).data)


class SharePost(GenericAPIView):
    """
    post:
    Share an existing Post with a comment.
    """
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_url_kwarg = 'post_id'
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        post_to_share = self.get_object()
        data = request.data

        # Create a new post that references the shared post
        shared_post = Post()
        shared_post.user = request.user
        shared_post.shared_post = post_to_share
        shared_post.content = data.get('content', '')  # Comment from the User
        shared_post.save()

        return Response(self.get_serializer(instance=shared_post).data, status=status.HTTP_201_CREATED)
